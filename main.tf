terraform {

  required_providers {

    aws = {

      source = "hashicorp/aws"

    }

  }

  required_version = ">= 0.12"

}

provider "aws" {

  region = "us-east-1"

}

resource "aws_s3_bucket" "daxko-test-2" {

  bucket = "daxko-test-2"


}

resource "aws_s3_bucket_lifecycle_configuration" "daxko-test-2" {

  bucket = aws_s3_bucket.daxko-test-2.id

  rule {
    id = "Delete files after 30 days."

    status = "Enabled"

    filter{}

    noncurrent_version_expiration {
      noncurrent_days = 30
    }
  }

}

resource "aws_s3_bucket_public_access_block" "app" {
  bucket = aws_s3_bucket.daxko-test-2.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true

}

resource "aws_iam_group" "daxko-devops-2" {
  name = "daxko_devops_2"


}

resource "aws_iam_group" "daxko-client-2" {
  name = "daxko_client_2"


}

resource "aws_iam_group_policy" "s3_policy_devops" {

  name = "s3_policy_devops"
  group = aws_iam_group.daxko-devops-2.name

  policy = jsonencode({
    Statement = [
      {

        Effect   = "Allow"

        Action = [
                "s3:CreateAccessPoint",
                "s3:PutAnalyticsConfiguration",
                "s3:PutAccelerateConfiguration",
                "s3:PutAccessPointConfigurationForObjectLambda",
                "s3:DeleteObjectVersion",
                "s3:ListBucketVersions",
                "s3:RestoreObject",
                "s3:DeleteAccessPoint",
                "s3:CreateBucket",
                "s3:ListBucket",
                "s3:DeleteAccessPointForObjectLambda",
                "s3:ReplicateObject",
                "s3:PutEncryptionConfiguration",
                "s3:DeleteBucketWebsite",
                "s3:AbortMultipartUpload",
                "s3:PutLifecycleConfiguration",
                "s3:UpdateJobPriority",
                "s3:DeleteObject",
                "s3:DeleteBucket",
                "s3:PutBucketVersioning",
                "s3:ListBucketMultipartUploads",
                "s3:PutIntelligentTieringConfiguration",
                "s3:PutMetricsConfiguration",
                "s3:PutBucketOwnershipControls",
                "s3:PutReplicationConfiguration",
                "s3:PutObjectLegalHold",
                "s3:InitiateReplication",
                "s3:UpdateJobStatus",
                "s3:PutBucketCORS",
                "s3:PutInventoryConfiguration",
                "s3:ListMultipartUploadParts",
                "s3:PutObject",
                "s3:PutBucketNotification",
                "s3:DeleteStorageLensConfiguration",
                "s3:PutBucketWebsite",
                "s3:PutBucketRequestPayment",
                "s3:PutObjectRetention",
                "s3:PutBucketLogging",
                "s3:CreateAccessPointForObjectLambda",
                "s3:PutBucketObjectLockConfiguration",
                "s3:ReplicateDelete"
        ]
        
        Resource = [
                "arn:aws:s3:::daxko-test-2",
                "arn:aws:s3-object-lambda:*:518973728361:accesspoint/*",
                "arn:aws:s3:::*/*",
                "arn:aws:s3:*:518973728361:storage-lens/*",
                "arn:aws:s3:*:518973728361:job/*",
                "arn:aws:s3:*:518973728361:accesspoint/*"
        ]
      }
    ]

  })

}


resource "aws_iam_group_policy" "s3_policy_client" {

  name = "s3_policy_client"
  group = aws_iam_group.daxko-client-2.name

  policy = jsonencode({
    Statement = [
      {

        Effect   = "Allow"

        Action = [
                "s3:GetObjectVersionTagging",
                "s3:GetStorageLensConfigurationTagging",
                "s3:GetObjectAcl",
                "s3:GetBucketObjectLockConfiguration",
                "s3:GetIntelligentTieringConfiguration",
                "s3:GetObjectVersionAcl",
                "s3:GetBucketPolicyStatus",
                "s3:GetObjectRetention",
                "s3:GetBucketWebsite",
                "s3:GetJobTagging",
                "s3:GetObjectAttributes",
                "s3:GetObjectLegalHold",
                "s3:GetBucketNotification",
                "s3:DescribeMultiRegionAccessPointOperation",
                "s3:GetReplicationConfiguration",
                "s3:ListMultipartUploadParts",
                "s3:GetObject",
                "s3:DescribeJob",
                "s3:GetAnalyticsConfiguration",
                "s3:GetObjectVersionForReplication",
                "s3:GetAccessPointForObjectLambda",
                "s3:GetStorageLensDashboard",
                "s3:GetLifecycleConfiguration",
                "s3:GetInventoryConfiguration",
                "s3:GetBucketTagging",
                "s3:GetAccessPointPolicyForObjectLambda",
                "s3:GetBucketLogging",
                "s3:ListBucketVersions",
                "s3:ListBucket",
                "s3:GetAccelerateConfiguration",
                "s3:GetObjectVersionAttributes",
                "s3:GetBucketPolicy",
                "s3:GetEncryptionConfiguration",
                "s3:GetObjectVersionTorrent",
                "s3:GetBucketRequestPayment",
                "s3:GetAccessPointPolicyStatus",
                "s3:GetObjectTagging",
                "s3:GetMetricsConfiguration",
                "s3:GetBucketOwnershipControls",
                "s3:GetBucketPublicAccessBlock",
                "s3:ListBucketMultipartUploads",
                "s3:GetAccessPointPolicyStatusForObjectLambda",
                "s3:GetBucketVersioning",
                "s3:GetBucketAcl",
                "s3:GetAccessPointConfigurationForObjectLambda",
                "s3:GetObjectTorrent",
                "s3:GetStorageLensConfiguration",
                "s3:GetBucketCORS",
                "s3:GetBucketLocation",
                "s3:GetAccessPointPolicy",
                "s3:GetObjectVersion"
        ]
        
        Resource = [
                "arn:aws:s3:::daxko-test-2",
                "arn:aws:s3-object-lambda:*:518973728361:accesspoint/*",
                "arn:aws:s3:::*/*",
                "arn:aws:s3:*:518973728361:storage-lens/*",
                "arn:aws:s3:*:518973728361:job/*",
                "arn:aws:s3:*:518973728361:accesspoint/*"
        ]
      }
    ]

  })

}